\documentclass{article}

\usepackage{amsmath,amssymb}
\usepackage{a4wide}
\usepackage{nameref,zref-xr}
\usepackage{xc}
\zxrsetup{toltxlabel}
\zexternaldocument*{estimates_dirichlet}
\externalcitedocument{estimates_dirichlet}

\title{Reply to the reviewer comments on the manuscript\\[1em] ``Error estimates for variational normal derivatives and Dirichlet control problems with energy regularization''}

\author{Max Winkler}

\begin{document}

\maketitle

\parskip1em
\parindent0pt

Dear referee, dear editor.

First, I would like to express my gratitude to the unknown referee for
carefully reading the manuscript and for giving very helpful remarks.
I revised the manuscript according to the suggestions. All changes in the text
are highlighted in blue. In the following I would like to respond to the reviewer comments:

\textit{a) The paper is written less clear than possible. For example notation
is introduced in a confusing way, e.g., $\mathcal E_h$ is simultaneously the zero extension, an
arbitrary extension, and the set of elements in the proof of Theorem 2. Moreover, the solution to the PDE is denoted by $u$ or $y$ and the Dirichlet data by $g$
and $z$.}

I understand the reviewer's arguments. Thus, I changed the notation as
suggested. The solution triple (control/state/adjoint) of the optimal control problem is now denoted
by $(z,u,p)$ as in \cite{OPS13}. This notation is also used in Section 3 now:
$u$ is the solution of the boundary-value problem and $z$ the boundary datum.
Moreover, I denote the harmonic extension by $S$ and not by $B$, because it is
also the solution operator of the state equation used in Section
4. Consequently, the harmonic extension is $S_h$ now and for the zero
extension I use $\tilde S_h$.
As also suggested, I avoid the usage of arbitrary extensions and directly insert what I need.

\textit{b) The most important space, namely $H^{1/2}(\Gamma)$ is not properly introduced. For
large parts of the manuscript the definition is $H^{1/2}(\Gamma)$ as trace of $H^1(\Omega)$.
But then there are parts which require implicitly that $H^{1/2}(\Gamma)$ is the ’0.5’-
interpolation between $L^2(\Gamma)$ and $H^1(\Gamma)$, requiring, at the very least, that $H^1$ is
defined on $\Gamma$, a construction which is not so clear to me on non-smooth boundaries. Moreover, as no condition is made to assert that the boundaries may not
touch I am certain that the two definitions will not be identical.}

I added a definition at the beginning of Section 2. I used a definition using
the Sobolev-Slobodeckij-norm. Due to the surjectivity of the trace operator
this space is equivalent to the natural trace space of $H^1(\Omega)$ in case
of Lipschitz domains. I also added a reference. The fact that this coincides
also with the interpolation space $[L^2(\Gamma), H^1(\Gamma)]_{1/2}$ is proved
in the book of McLean. I added the desired references to the text.
\begin{itemize}

\item \textit{p1,l3 below A definition of $H^{1/2}(\Omega)$ should be given.}

I think the reviewer means $H^{1/2}(\Gamma)$ and not $H^{1/2}(\Omega)$. I
added the missing definition, but not in the introduction but at the beginning
of Section 2 to avoid too many details in the introduction. I
hope this is OK.

\item \textit{(1) et.seq. Here you introduce $u$ as the solution to (2) with Dirichlet data $z$. I would
recommend to keep this notation throughout - and not change to $y$ and
$g$ intermittently. Further, as in the optimal control community the notations $(u, q, z)$ and $(y, u, p)$ are frequently used for state-control-adjoint I
would recommend to reconsider using either one of these notations - or a
completely different one but not some mixture as it makes the manuscript
less accessible.}

This is correct. As explained in (a) I use the notation $(z,u,p)$ now.

\item \textit{p2 In the literature review. The point of using more regular controls in
the Dirichlet-control case is much older. For example Gunzburger et.al.
(M2AN, 1991) used an $H^1$ regularization avoiding the difficulties of the
$L^2$ control costs. Also Mateos, Neitzel (COAP 2016) derived similar estizmates (on convex domains) for Dirichlet control with state constraints
which should coincide with yours in the limit $p\to 2$ (or if not a comment
would be welcome).}

I added references to the two mentioned articles in the introduction.
I also added a brief comparison between $L^2(\Gamma)$- and
$H^{1/2}(\Gamma)$-regularization regarding convergence behavior at
the end of this article.
% However, in the present article only estimates in the $H^{1/2}(\Gamma)$-norm are
% derived. Estimates in $L^2(\Gamma)$ have not been shown in the present paper,
% but the experiments show that the rate is obviously increased by $1/2$
% compared to the $H^{1/2}(\Gamma)$-norm.

One additional remark: In Mateos/Neitzel \cite{MateosNeitzel2016} also $L^2(\Gamma)$-regularization is considered and the rate
$\min\{1,\lambda/2\}-\varepsilon$ is proved which is probably not sharp for
$\omega_{\max}>\pi/2$, at least for the case that the state constraints are not active.
The sharp rate of $\min\{1,\lambda-1/2\}-\varepsilon$ for unconstrained
Dirichlet control problems can be found in \cite{AMPR16}.
The rate we observe in the $L^2(\Gamma)$-norm is
$\min\{2,\lambda+1/2-\varepsilon\}$ and is obviously higher by $1$.
This is not so surprising, because the
solutions are more regular when $H^{1/2}(\Gamma)$-regularization is used.

\item \textit{p2,l1 below Now you redefine $y = u$ for no apparent reason.}

I fixed this as explained in (a).

\item \textit{p3,l2 I do not understand your assumption. How do you assert continuity of
the normal derivative by the data?}

This is a good question and the article is not accurate enough concerning this
issue. I added a remark to this on page 3. In the application we have in mind
(Dirichlet control), see in particular the 2nd and 4th term of the estimate in
Lemma 12,
the normal derivative of $\partial_n p$ is zero in the
corners (and thus continuous) as $p$ fulfills homogeneous Dirichlet conditions. This property is
also transferred to $\partial_n (Sz) = Nz = -\frac1\nu \partial_n p$. I added a
brief remark in the introduction and a short proof to Theorem 3, where these
arguments are explained.

Nevertheless, for general problems with inhomogeneous Dirichlet conditions (consider e.g.\ $-\Delta u=0$ in $\Omega$ and $u|_\Gamma = x_1+x_2$ for the unit square $\Omega:=[0,1]^2$, the exact solution is $u=x_1+x_2$) the exact normal derivative can be discontinuous and an approximation by continuous functions would not make so much sense.
In this case it would be better to compute a variational normal derivative for each boundary edge
separately.
The results of Theorems 1 and 2 would remain true for these piecewise approximations as well.

\item \textit{p3,l5 It would be good to give a definition of $\partial_n^h$
here. It is very inconvenient that the definition for these symbols follows on p13.}

I added the missing definition as suggested.

\item \textit{p3,l20 that $\to$ than}

\textit{Fixed}

\item \textit{p3,l1 below $R \to R^2$}

\textit{Fixed}

\item \textit{p6,l3 I think for this it would be good to define the 3/2 Version of this space as
you need it later on.}

Analogous to the space $H^{1/2}(\Gamma)$, the space
$V^{3/2,2}_{\vec\beta}(\Gamma)$ can be defined as the natural trace space of
$V^{2,2}_{\vec\beta}(\Omega)$. One can also define an equivalent space over a weighted
Sobolev-Slobodeckij-norm. I introduce this norm on page 6 now.

\item \textit{p6,l1 below I believe in the sum you mean
$\Lambda^{k,\sigma}_{\delta_j}$ and $\hat\Omega_{R/2}$ instead of $\Lambda_\delta^{k,\sigma}$ and $\Omega_R/2$}

Yes, Fixed.

\item \textit{p7 I found the proof of Lemma 4 a bit unclear. I figured that you introduce
the polynomials to assert suitable ’zero’ boundary conditions to apply
the results of Maz’ya et.al.; it might be suitable to mention this as it is
otherwise not very clear why removing a regular function would help in
showing regularity.}

This is correct. I explain the ideas of the proof more accurately now, see the beginning
of the proof of Lemma 4.

\item \textit{p8,l3 duplicate ’we’}

Fixed.

\item \textit{p8,l9 I think you mean $q_{\lfloor 1-\nu_j\rfloor}$  instead of
$q_{1-\nu_j}$?}

The numbers $\nu_j$ are integers. I forgot to mention this important fact. At the first
occurrence of $\nu_j$ I now say $\nu_j\in \mathbb Z$.

\item \textit{p10,l8 below This requires the assumption that $h$ is sufficiently
small.}

I added this as a general assumption at the beginning of Section 3, where the
FE mesh is introduced.

\item \textit{p11,l6 I assume $V^{3/2} = V^{3/2,2}$?}

Yes, fixed.

\item \textit{p11,(18) As mentioned above a definition of $V^{3/2,2}$ is missing, 
so I can only guess
that the norm is non-local. But then the calculation $\sum_E \|x\|_E^2 \simeq
\|x\|_{\cup E}^2$
is typically not correct. I would expect that the desired result follows by
interpolating suitable integer versions of the space.}

This is a good question which requires a more detailed discussion. Of course, $V^{3/2,2}$ is
non-local, but in the proof I applied only the estimate $\sum_{E\in\mathcal E_h}
|u|_{V^{3/2,2}_{\vec\beta}(E)}\le |u|_{V^{3/2,2}_{\beta}(E)}$ which is the
direction which is true. Only the reverse direction is wrong (because the
double-integral becomes a double-sum after a splitting into boundary
elements). I added a remark on this in the proof of Lemma 5.

\item \textit{p11,l15 For the interpolation argument, you need that $H^{1/2}(\Gamma) = (L^2(\Gamma), H^1(\Gamma))_{0.5}$
for some interpolation $(, )_\Theta$. I would like to see a reference or argument
that this is correct for the space $H^{1/2}(\Gamma) = \text{tr}(H^1(\Omega))$, used otherwise
in this paper, on a nonconvex polygonal domain.}

The fact that the Sobolev-Slobodeckij space
$H^{1/2}(\Gamma)$ is equivalent to $[L^2(\Omega),H^1(\Omega)]_{1/2}$ can be
found in the book of McLean. I added this reference also to the proof of Lemma 5, where this argument is needed.

\item \textit{p13,l13 Depending on the choice of $H^{1/2}$ this is either
trivial or needs a reference.}

I added a reference to this result.

\item \textit{p14,l20 The operator $\mathcal E_h$ is already defined to be the zero extension. I would
suggest to directly insert the desired extension operator.}

This is a good idea. Now, I directly insert the discrete harmonic extension
$B_h$ (now: $S_h$).

\item \textit{p21,l9 I would suggest to define the harmonic extension operator B earlier - an
not just implicitly by $\mathcal E_h$ . Further, this is yet another definition for the
symbol $\mathcal E_h$.}

I added a definition of $B$ (now: $S$) before (49). Moreover, I apply (23) with a
different extension. This requires a further explanation that has also been
added to the text.

\item \textit{p21,14 Depending on the answer to p11,(18) the argument for (50) needs to be
refined.}

This is correct. In the last version the validity of (50) was not obvious. We
apply Lemma 5 where an interpolation error estimate for $I_h$ is shown. I
added an explanation why $\|u-Q_h u\|_{H^{1/2}(\Gamma)} \le c\,\|u-I_h
u\|_{H^{1/2}(\Gamma)}$ holds.

\item \textit{p21 I found the wording of the two paragraphs after (50) confusing. It is not
always clear if you are referring to the dual solution in Lemma 9, or to
the current proof.}

This paragraph was indeed confusing. I wanted to keep the proof of this
estimate as short as possible as the arguments are, with a few modifications,
exactly the same as in (44) and (46). Now, I decided to repeat these arguments
for better readability of this article. 

\item \textit{p23,(54) Why does your operator T have an superscript $\nu$? In my opinion there
are already enough indices to be kept in mind, and you don't investigate
the dependence on $\nu$ so I would suggest to drop the superscript.}

The notation is taken from \cite{OPS13}. But the referee is right. I drop
the subscript $\nu$.

\item \textit{p23,(55) The last ',' should be a '.'}

Fixed.

\item \textit{p24,(61) Why is $T_\nu$ coercive?}

I added an explanation after Equation \eqref{eq:variational_form}.

\end{itemize}



\end{document}
